#!/bin/sh

WEEKDAY=$(date +%A)
END=${1:-1}

for i in `seq 1 $END`
do
	echo "Hello $USER. Today is $WEEKDAY."
done
